# Попросить пользователя ввести текст.
# В результате вывести процент букв в верхнем
# регистре (заглавные) и в нижнем регистре (прописные).

# name = input("Введите текст:")

message = input("Type word: ")

length_ = len(message) - message.count(' ')
upper_let = sum(1 for c in message if c.isupper())
lower_let = sum(1 for c in message if c.islower())

print("upper:", + (upper_let/length_)*100, "%")
print("lower:", + (lower_let/length_)*100, "%")

